# lancelot-user 12 SP1A.210812.016 V13.0.2.0.SJCMIXM release-keys
- manufacturer: xiaomi
- platform: mt6768
- codename: galahad
- flavor: lancelot-user
- release: 12
- id: SP1A.210812.016
- incremental: V13.0.2.0.SJCMIXM
- tags: release-keys
- fingerprint: Redmi/galahad_global/galahad:12/SP1A.210812.016/V13.0.2.0.SJCMIXM:user/release-keys
Redmi/lancelot_global/lancelot:12/SP1A.210812.016/V13.0.2.0.SJCMIXM:user/release-keys
Redmi/lancelot/lancelot:12/SP1A.210812.016/V13.0.2.0.SJCMIXM:user/release-keys
- is_ab: false
- brand: Redmi
- branch: lancelot-user-12-SP1A.210812.016-V13.0.2.0.SJCMIXM-release-keys
- repo: redmi_galahad_dump
